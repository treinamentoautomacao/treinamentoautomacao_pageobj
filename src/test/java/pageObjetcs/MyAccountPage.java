package pageObjetcs;

import org.openqa.selenium.support.PageFactory;
import utils.Browser;

public class MyAccountPage extends MyAccountElementMapper {
    public MyAccountPage(){
        PageFactory.initElements(Browser.getCurrentDriver(), this);

    public boolean isPageMyAccount(){
        return getMyAccount.contains("MY ACCOUNT");
        }
    public String getMyAccount(){
        return myAccount.getText();
        }
    }

}
