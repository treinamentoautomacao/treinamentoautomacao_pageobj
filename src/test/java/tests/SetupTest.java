package tests;

import com.sun.org.glassfish.gmbal.Description;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import pageObjetcs.*;
import utils.Browser;
import utils.Utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

//@Feature("Fluxos de Teste")
public class SetupTest extends BaseTests {

    @Test
   // @Description("Teste de Abrir o Browser")
    public void testOpeningBrowserAndLoadingPage(){
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl()));
        System.out.println("Abrimos o navegador");
    }

    @Test
   // @Description("Teste que realiza login")
    public void testLogin(){
        //iniciar páginas
        HomePage home = new HomePage();
        LoginPage login = new LoginPage();
        //MyAccountPage myAccount = new MyAccountPage();

        //clicar botao login Home
        home.clickBntLogin();
        System.out.println("Clicamos botao login");

        /*ver se foi pra pagina login
        //assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains("my-account"));

        //Realizar login
        login.doLogin();
        System.out.println("Realizamos login");

        //verificar se foi pra minha conta
        assertTrue(MyAccount.isPageMyAccount());
        System.out.println("Minha conta");

         */


        //Browser.getCurrentDriver().findElement(By.className("login")).click();
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl().concat("index.php?controller=authentication&back=my-account")));
        /*
        Browser.getCurrentDriver().findElement(By.id("email")).sendKeys("janainaslima@yahoo.com.br");
        Browser.getCurrentDriver().findElement(By.id("passwd")).sendKeys("12345");
        Browser.getCurrentDriver().findElement(By.id("SubmitLogin")).click();
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl().concat("index.php?controller=my-account")));
        assertTrue(Browser.getCurrentDriver().findElement(By.className("page-heading")).getText().contains("MY ACCOUNT"));
         */
        login.fillEmail();
        System.out.println("Preencheu email");
        login.fillPasswd();
        System.out.println("Preencheu senha");
        login.clickBtnSubmitLogin();
        System.out.println("Sign In");

        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl().concat("index.php?controller=my-account")));
        System.out.println("validou url minha conta");

        assertTrue(Browser.getCurrentDriver().findElement(By.className("page-heading")).getText().contains("MY ACCOUNT"));
        System.out.println("validou conta no site");
    }

    @Test

    public void testSearch(){

        String quest = "dress";
        String questResultQtd = "7";

        HomePage home = new HomePage();
        SearchPage search = new SearchPage();

        home.doSearch(quest);

        assertTrue(search.isSearchPage());
        assertEquals(search.getTextLighter(), "dress");
        assertEquals(search.getTextHeading_counter(), CoreMatchers.containsString(questResultQtd));

    }


    @Test
    public void testAcessCategoryTShirts() {
        /*
        Browser.getCurrentDriver().findElement(By.id("search_query_top")).sendKeys("dress");
        Browser.getCurrentDriver().findElement(By.name("submit_search")).click();
        Browser.getCurrentDriver().findElement(By.linkText("DRESSES"));
    }
      */
        //iniciar página
        HomePage home = new HomePage();
        CategoryPage category = new CategoryPage();

        //clicar na categoria
        home.clickCategoryTShirts();

        assertTrue(category.isPageTShirts());
    }
    @Test
    public void testAddProductToProductPage(){
        //acessar categoria tshirts
        testAcessCategoryTShirts();

        //iniciar pagina
        CategoryPage category = new CategoryPage();
        ProductPage pdp = new ProductPage();

        //salva nome produto
        String nameProductCategory = category.getProductNameCategory();

        // clicar em more e direcionar para pagina do produto
        category.clickProdutoAddToProductPage();

        //ver se produto esta na pagina detalhes
        assertTrue(pdp.getProductNamePDP().equals(nameProductCategory));
    }

    @Test
    public void testAddProductToCartPage(){

        // acessar pagina
        testAddProductToProductPage();

        //iniciar as paginas
        ProductPage pdp = new ProductPage();
        CartPage cart = new CartPage();

        //salvar nome produto na pagina pdp
        String nameProductPDP = pdp.getProductNamePDP();

        //clicar no botao add carrinho
        pdp.clickButtonAddToCart();

        //clicar no botao proceed to checkout
        pdp.clickButtonModalProceedToCheckout();

        assertTrue(cart.getNameProductCart().equals(nameProductPDP));

    }
}
